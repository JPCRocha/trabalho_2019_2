package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private int id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;
    private List<Inscricao> inscricoes = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<AreaCientifica> areasCientificas = new ArrayList<>();

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        super();
        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);

        }
        this.areasCientificas.add(areaCientifica);
        professor.adicionarSeminario(this);
        this.professores.add(professor);
        this.qtdInscricoes = qtdInscricoes;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public boolean isMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areasCientificas;
    }
}
