package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Long id;
    private Boolean direitoMaterial;
    private Seminario seminario;
    private Estudante estudante;
    private SituacaoInscricaoEnum situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;

    public Inscricao(Seminario seminario) {
        super();
        this.seminario = seminario;
        this.seminario.getInscricoes().add(this);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacaoInscricaoEnum;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void cancelarCompra() {
        this.estudante.removerInscricao(this);
        this.estudante = null;
        this.direitoMaterial = null;
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;

    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
        this.estudante = estudante;
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;
        this.estudante.adicionarInscricao(this);
    }

    public void realizarCheckIn() {
        this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
    }

}
